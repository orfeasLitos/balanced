// https://shane-o.dev/blog/aiming-for-idiomatic-rust

use std::str::Chars;

pub fn is_balanced(input: &str) -> bool {
    is_balanced_rec(None, &mut input.chars())
}

fn is_balanced_rec(other: Option<char>, input: &mut Chars) -> bool {
    loop {
        let c = input.next();
        let no_problems = match c {
            Some('(') => is_balanced_rec(Some(')'), input),
            Some('[') => is_balanced_rec(Some(']'), input),
            Some('{') => is_balanced_rec(Some('}'), input),
            Some(')') | Some(']') | Some('}') | None => { return c == other },
            _ => true,
        };
        if !no_problems { return false }
    }
}

#[test]
fn balanced_1() {
    assert!(is_balanced("()"));
}

#[test]
fn balanced_2() {
    assert!(is_balanced("(){}[]"));
}

#[test]
fn balanced_3() {
    assert!(is_balanced("{([])}"));
}

#[test]
fn imbalanced_4() {
    assert!(!is_balanced("{ (`([]op)[)]p}"));
}

#[test]
fn imbalanced_1() {
    assert!(!is_balanced("{ (`([]op)[)p}"));
}

#[test]
fn imbalanced_2() {
    assert!(!is_balanced("{([])"));
}

#[test]
fn imbalanced_3() {
    assert!(!is_balanced("([])}"));
}
